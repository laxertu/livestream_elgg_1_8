<?php

// @translated by psy (https://lorea.cc)

$labels = array( 
	'livestream:new_item'  =>  "New stream", 
	'livestream:src'  =>  "URL", 
	'livestream:width'  =>  "W",
	'livestream:height'  =>  "H",
	'livestream:save'  =>  "Save",
	'livestream:back'  =>  "Back",
	'livestream:livestream'  =>  "Livestream",
	'livestream:enable'  =>  "enable Livestream",
	'livestream:permission_denied' => 'operation not permitted',
	'livestream:internal_error' => 'Internal error',
	'livestream:error:delete' => 'Error while deleting a stream',
	'livestream:success:delete' => 'Stream deleted successfully',
	'livestream:delete' => 'Delete',
	'livestream:delete:ask' => 'Are you sure?',
	'livestream:viewall' => 'View all'
	
); 

add_translation('it', $labels); 

?>
